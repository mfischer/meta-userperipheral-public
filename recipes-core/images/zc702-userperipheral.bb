DESCRIPTION = "A console-only image with stuff for userperipheral development on zc702 boards."

IMAGE_FEATURES += "splash ssh-server-openssh tools-sdk \
                   tools-debug debug-tweaks dev-pkgs package-management"

CORE_IMAGE_EXTRA_INSTALL = "\
    kernel-modules \
    libudev \
    boost \
    user-peripheral \
    "

inherit core-image
